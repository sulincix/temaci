Gtk.Window mainWindow;
string curpkg;
int main(string args[]){
	Gtk.init(ref args);
	mainWindow=get_window();
	Gtk.Button arr[999];
	var home=Environment.get_variable("HOME");
	string[] packages=readfile(home+"/.temaci/index").split("\n");
	print(packages.length.to_string());
	for(int i=0;i<packages.length-1;i++){
		curpkg=packages[i];
		string[] ctx=readfile(home+"/.temaci/"+packages[i]).split("\n");
		var name="";
		var icon="";
		for(int j=0;j<ctx.length;j++){
			var attr=ctx[j].split(":");
			if(attr[0]=="Name"){
				name=attr[1];
			}else if(attr[0]=="Icon"){
				icon=home+"/.temaci/"+attr[1];
			}
		}
		arr[i]=new Gtk.Button();
		arr[i].add(item(name,icon));
		arr[i].clicked.connect(()=>{
			var infoWin=info(home+"/.temaci/"+curpkg);
			mainWindow.hide();
			infoWin.destroy.connect(()=>{
				mainWindow.show_all();
			});
			infoWin.show_all();
		});
	}
	var table=new Table();
	for(int i=0;i<arr.length;i++){
		table.set_item(1,i,arr[i]);
	}
	table.init(10,10);
	mainWindow.add(table.main);
	mainWindow.show_all();
	Gtk.main();
	return 0;
}
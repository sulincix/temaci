class Table {
public Gtk.Box main=null;
public Gtk.Widget container[1000];
private int hsize=0;
private int wsize=0;
	public void init(int h,int w){
		hsize=h;
		wsize=w;
		main=new Gtk.Box(Gtk.VERTICAL,1);
		Gtk.Box lines[1000];
		for(int i=0;i<h;i++){
			lines[i]=new Gtk.Box(Gtk.HORIZONTAL,1);
			main.pack_start(lines[i]);
		}
		for(int i=0;i<h;i++){
			for(var j=0;j<w;j++){
				if(container[i*h+j] == null){
					container[i*h+j]=new Gtk.Label("");
				}
				lines[i].pack_start(container[i*h+j],true);
			}
		}
	}
	public Gtk.Widget get_item(int x,int y){
		return container[x*hsize+y];
	}
	public void set_item(int x,int y,Gtk.Widget item){
		container[x*hsize+y]=item;
	}
}
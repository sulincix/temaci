
public Gtk.Window get_window(){
	var w=new Gtk.Window();
	w.show_all();
	w.destroy.connect(Gtk.main_quit);
	w.set_size_request(700,400);
	w.set_titlebar(new Gtk.HeaderBar());
	return w;
}
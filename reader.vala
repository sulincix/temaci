public static string readfile (string path) {
	File file = File.new_for_path (path);
	var ret="";
	try {
		FileInputStream @is = file.read ();
		DataInputStream dis = new DataInputStream (@is);
		string line;

		while ((line = dis.read_line ()) != null) {
			ret=ret+line+"\n";
		}
	} catch (Error e) {
		print ("Error: %s\n", e.message);
	}

	return ret;
}
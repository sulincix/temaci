using WebKit;
Gtk.Window info(string file){
	var infoWindow=new Gtk.Window();
	infoWindow.set_size_request(700,400);
	var home=Environment.get_variable("HOME");
	string[] ctx=readfile(file).split("\n");
	var name="";
	var icon="";
	var info="";
	for(int j=0;j<ctx.length;j++){
			var attr=ctx[j].split(":");
			if(attr[0]=="Name"){
				name=attr[1];
			}else if(attr[0]=="Icon"){
				icon=home+"/.temaci/"+attr[1];
			}else if(attr[0]=="Info"){
				info=home+"/.temaci/"+attr[1];
			}
		}
	print(info);
	var webkit= new WebView();
	var vbox=new Gtk.Box(Gtk.VERTICAL,1);
	var hbox=new Gtk.HeaderBar();
	var inst=new Gtk.Button.with_label("install");
	var goback=new Gtk.Button.with_label("<");
	Gtk.Image logo = new Gtk.Image();
	logo.set_from_file(icon);

	infoWindow.set_titlebar(hbox);
	hbox.pack_start(goback);
	hbox.pack_start(logo);
	hbox.pack_start(new Gtk.Label(name));
	hbox.pack_end(inst);
	vbox.pack_end(webkit);

	webkit.load_html(readfile(info),null);
	goback.clicked.connect(()=>{
		infoWindow.destroy(); 
	});
	infoWindow.add(vbox);
	return infoWindow;
}